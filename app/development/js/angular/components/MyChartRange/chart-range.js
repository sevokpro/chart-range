(function () {
    angular.module('app').directive('myChartrange', [function () {
        return {
            templateUrl: '/js/angular/components/MyChartRange/chart-range.html',
            replace: true,
            restrict: 'E',
            scope: {
            	data: '@',
                uplink: '@'
            },
            controller: function($scope, $element, $attrs, $http) {
            	
            	var agent = navigator.userAgent;
				var ie = (/MSIE/).test(agent) || (/Trident/).test(agent);

            	//init object
				$scope.slider = {};

				$scope.slider.uplink = $scope.uplink;


				//function which calculate left and right range
				$scope.slider.calculate_ranges = function (data) {
					data = data || $scope.slider.data;

					var max_value = Math.max.apply(null, data);
					var min_value = Math.min.apply(null, data);		
					
					var old_min_value = $($scope.slider.DOM_link).slider( "option", "min");
					var old_max_value = $($scope.slider.DOM_link).slider( "option", "max");


					old_min_value = ( typeof(old_min_value) == 'object' ) ? min_value : old_min_value;
					old_max_value = ( typeof(old_max_value) == 'object' ) ? max_value : old_max_value;

					var all_length = Math.abs(old_max_value) + Math.abs(old_min_value);

					var right_residue = Math.abs(old_max_value - max_value);
					var left_residue = Math.abs(old_min_value - min_value);

					var right_residue_by_percent = right_residue / all_length;
					var left_residue_by_percent = left_residue / all_length;

					var out_left = old_min_value;
					var out_right = old_max_value;

					if ( ( min_value <= old_min_value ) || ( left_residue_by_percent > 0.15  ) ){
						out_left = min_value;
						left_residue_by_percent = 0;

					}

					if (( max_value >= old_max_value) || ( right_residue_by_percent > 0.15  )) {
						out_right = max_value;
						right_residue_by_percent = 0;
					}

					if ( left_residue_by_percent < 0.05 ) {
						out_left = ~~( out_left - all_length * 0.1 );
					}

					if (right_residue_by_percent < 0.05) {
						out_right = ~~( out_right + all_length * 0.1 );

					}

					return {
						min: out_left,
						max: out_right
					};
				};

				//function which update ranges if they are changes
				$scope.slider.update_slider_ranges = function (data) {
					var old_min_value = $($scope.slider.DOM_link).slider( "option", "min");
					var old_max_value = $($scope.slider.DOM_link).slider( "option", "max");

					var new_values = $scope.slider.calculate_ranges(data);

					if ( old_min_value !== new_values.min ){
						$($scope.slider.DOM_link).slider( "option", "min", new_values.min );
					}
					if ( old_max_value !== new_values.max ) {
						$($scope.slider.DOM_link).slider( "option", "max", new_values.max );
					}
				};
			
				//init data
				$scope.slider.get_data = function () {
					var data = $scope.data;
					data =JSON.parse(data);
					return data
				}
				$scope.slider.data = $scope.slider.get_data();

				//init ranges
				$scope.slider.ranges = $scope.slider.calculate_ranges();
				
				//set DOM element
				$scope.slider.DOM_link = $($element).children('#slider');

				
				//get tooltip template
				$scope.slider.get_tooltip_template = function (index) {
					var tooltip_tmpl = [
					'<span class="tooltip-wrapper">',
						'<input ',
							'type="number" ',
							'class="range-chart-tooltip ',(ie) ? 'explorer-tooltip-support' : '','" ',
							'value="',$scope.slider.data[index],'"',
						' />',
						'<div class="tooltip-after ',(ie) ? 'explorer-after-support' : '','"></div>',
					'</span>'
						].join('');
					return tooltip_tmpl;
				};

				//get point template
				$scope.slider.get_point_template = function (index) {
					var point_template = [
						'<span class="point">',
						index + 1,
						'</span>',
						$scope.slider.get_tooltip_template(index)
						].join('');
					return point_template;
				};		

				//function-constructor for slider
				$scope.slider.create = function (event, ui) {

					//get slider points
					var childrens = $($scope.slider.DOM_link).children();

					//correct slider oints, create tootlips for each point
					for (var i = 0; i < childrens.length; i++){
						//correct text on point
						$(childrens[i]).css({
							'text-align': 'center'
						});
						//create tooltip
						$(childrens[i]).html(
							$scope.slider.get_point_template(i)
							);
					}

					//add event listner for change tooltip
					$($scope.slider.DOM_link).on('keyup change', 'input', $scope.slider.tooltip_change_event);
				};

				$scope.slider.tooltip_change_event = function (e) {
					//get new data
					new_data = $scope.slider.coollect_data_from_tooltips();
					//check new data for errors
					for (var i = 0; i < new_data.length - 1; i++) {
						if (new_data[i] >= new_data[i + 1]){
							console.log('ошибка!');
							return false;
						}
					}
					//var new_ranges = $scope.slider.calculate_ranges(new_data);
					//update clider data if new data correct
					$scope.slider.update_slider_ranges(new_data);

					$($scope.slider.DOM_link).slider( "option", "values", new_data );
				};

				//collect data from tooltips
				$scope.slider.coollect_data_from_tooltips = function () {
					var out = [];
					var childrens = $($scope.slider.DOM_link).find('input');
					for (var i = 0; i < childrens.length; i++) {
						out.push(+$(childrens[i]).val());
					}
					return out;
				};

				//function which check points for correct input 
				//!!! and update tooltip
				$scope.slider.slide_move = function (event, ui) {
					//check new values
					for (var i = 0; i < ui.values.length - 1; i++) {
						if (ui.values[i] >= ui.values[i + 1]){
							return false;
						}
					}
					//update tooltip
			    	var elem = $(ui.handle).find('input');
			    	elem.val(ui.value);	
				};

				$scope.slider.save = function () {
					var data = $($scope.slider.DOM_link).slider( "option", "values");
					//data = JSON.stringify(data);

					var req = {
                        method: 'POST',
                        url: $scope.slider.uplink,
                        headers: {
                            'Content-Type': 'JSON'
                        },
                        data: {
                            d: JSON.stringify(data) 
                        }
                    }

                    $http(req).then(function successCallback(response) {
                        console.log('данные успешно отправлены на сервер');
                    }, function errorCallback(response) {
                        console.log('при отправке данных произошла ошибка');
                        console.log(response)
                    });
				};

                //init slider
                $($scope.slider.DOM_link).slider({
					min: $scope.slider.ranges.min,
					max: $scope.slider.ranges.max,
					step: 1,
					animate: "slow",
					values: $scope.slider.data,
					create: $scope.slider.create,
					slide: $scope.slider.slide_move,
					change: function (event, ui) {
						var root = this;
						if (this.stop === true) {
							return;
						} else {
							$scope.slider.update_slider_ranges(ui.values);
							this.stop = true;
							setTimeout(function() {
								root.stop = false;
							}, 200);
						}
					}
				});
            },
            // compile: function compile(tElement, tAttrs, transclude) {
            //     return function postLink(scope, iElement, iAttrs, controller) {
    
            //     }
            // },
            link: function postLink(scope, iElement, iAttrs) {
                
            }
        };
    }]);
})(); 